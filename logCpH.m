function tbl = logCpH(params, solution, titration, titr_normal, ...
  sample_volume, output)
  % Get acid/base system type
  sys = params{1};

  % Vary pH from 0 to 14 by 0.25 increments
  pH = (0:0.25:14)';
  H = 10 .^ -pH;
  logH = log10(H);
  pKow = 14;
  logOH = -(pKow + logH);
  OH = 10 .^ logOH;

  switch sys
    case 'Weak, monoprotic acid'
      Ct = str2num(params{3});
      pKa = str2num(params{4});
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H + Ka(1));
      alpha0 = H ./ d;
      alpha1 = Ka ./ d;
      logHA = log10(alpha0 .* Ct);
      logA = log10(alpha1 .* Ct);
      LHS = logH; % Left-hand side of charge balance
      RHS = log10(alpha1 .* Ct + OH); % Right-hand side of charge balance
      plt = plot(pH, logH, pH, logOH, pH, logHA, pH, logA);
      lgd_str = {'log H^+' 'log OH^-' 'log HA' 'log A^-'};
      tbl = table(pH, round(logH, 2), round(logOH, 2), round(logHA, 2), ...
        round(logA, 2), 'VariableNames', {'pH' 'log_H' 'log_OH' 'log_HA' ...
        'log_A'});
    case 'Weak, diprotic acid'
      Ct = str2num(params{3});
      pKa = [str2num(params{4}) str2num(params{5})];
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H .^ 2 + Ka(1) .* H + Ka(1) .* Ka(2));
      alpha0 = H .^ 2 ./ d;
      alpha1 = Ka(1) .* H ./ d;
      alpha2 = Ka(1) .* Ka(2) ./ d;
      logH2A = log10(alpha0 .* Ct);
      logHA = log10(alpha1 .* Ct);
      logA = log10(alpha2 .* Ct);
      LHS = logH; % Left-hand side of charge balance
      RHS = log10(alpha1 .* Ct + 2 * alpha2 .* Ct + OH); % Right-hand side of charge balance
      plt = plot(pH, logH, pH, logOH,pH, logH2A, pH, logHA, pH, logA);
      lgd_str = {'log H^+' 'log OH^-' 'log H_2A' 'log HA' 'log A^-'};
      tbl = table(pH, round(logH, 2), round(logOH, 2), round(logH2A, 2), ...
        round(logHA, 2), round(logA, 2), 'VariableNames', {'pH', 'log_H', ...
        'log_OH','log_H2A', 'log_HA', 'log_A'});
    case 'MB salt (monoprotic acid/base system)'
      Ct = str2num(params{3});
      pKa = str2num(params{4});
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H + Ka(1));
      alpha0 = H ./ d;
      alpha1 = Ka ./ d;
      logHA = log10(alpha0 .* Ct);
      logA = log10(alpha1 .* Ct);
      logM = log10(Ct);

      % Left- and right-hand sides of charge balance with metal cation
      % LHS = log10(H + Ct);
      % RHS = log10(alpha1 .* Ct + OH);
      % logM = repmat(logM, length(pH), 1);
      % plt = plot(pH, logH, pH, logOH, pH, logHA, pH, logA, pH, logM);
      % lgd_str = {'log H^+' 'log OH^-' 'log HA' 'log A^-' 'log M+'};

      % Left-hand side of charge balance without metal cation
      LHS = log10(H + alpha0 .* Ct);
      RHS = logOH;
      plt = plot(pH, logH, pH, logOH, pH, logHA, pH, logA);
      lgd_str = {'log H^+' 'log OH^-' 'log HA' 'log A^-'};
      tbl = table(pH, round(logH, 2), round(logOH, 2), round(logHA, 2), ...
        round(logA, 2), 'VariableNames', {'pH' 'log_H' 'log_OH' 'log_HA' ...
        'log_A'});
    case 'MB salt (diprotic acid/base system)'
      Ct = str2num(params{3});
      pKa = [str2num(params{4}) str2num(params{5})];
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H .^ 2 + Ka(1) .* H + Ka(1) .* Ka(2));
      alpha0 = H .^ 2 ./ d;
      alpha1 = Ka(1) .* H ./ d;
      alpha2 = Ka(1) .* Ka(2) ./ d;
      logH2A = log10(alpha0 .* Ct);
      logHA = log10(alpha1 .* Ct);
      logA = log10(alpha2 .* Ct);
      logM = repmat(log10(Ct), length(pH), 1);

      % Left- and right-hand sides of charge balance with metal cation
      % LHS = log10(H + Ct);
      % RHS = log10(alpha1 .* Ct + 2 .* alpha2 .* Ct + OH);
      % plt = plot(pH, logH, pH, logOH, pH, logH2A, pH, logHA, pH, logA, pH, ...
      %   logM);
      % lgd_str = {'log H^+' 'log OH^-' 'log H2A' 'log HA' 'log A^-' 'log M+'};

      % Left-hand side of charge balance without metal cation
      LHS = log10(H + alpha0 .* Ct);
      RHS = log10(OH + alpha2 .* Ct);
      plt = plot(pH, logH, pH, logOH,pH, logH2A, pH, logHA, pH, logA);
      lgd_str = {'log H^+' 'log OH^-' 'log H_2A' 'log HA' 'log A^-'};
      tbl = table(pH, round(logH, 2), round(logOH, 2), round(logH2A, 2), ...
        round(logHA, 2), round(logA, 2), round(logM, 2), 'VariableNames', ...
        {'pH', 'log_H', 'log_OH','log_H2A', 'log_HA', 'log_A', 'log_M'});
    case 'M2B salt (diprotic acid/base system)'
      Ct = str2num(params{3});
      pKa = [str2num(params{4}) str2num(params{5})];
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H .^ 2 + Ka(1) .* H + Ka(1) .* Ka(2));
      alpha0 = H .^ 2 ./ d;
      alpha1 = Ka(1) .* H ./ d;
      alpha2 = Ka(1) .* Ka(2) ./ d;
      logH2A = log10(alpha0 .* Ct);
      logHA = log10(alpha1 .* Ct);
      logA = log10(alpha2 .* Ct);
      logM = repmat(log10(2 .* Ct), length(pH), 1);

      % Left- and right-hand sides of charge balance with metal cation
      % LHS = log10(H + 2 .* Ct);
      % RHS = log10(alpha1 .* Ct + 2 .* alpha2 .* Ct + OH);
      % plt = plot(pH, logH, pH, logOH, pH, logH2A, pH, logHA, pH, logA, ...
      %   pH, logM);
      % lgd_str = {'log H^+' 'log OH^-' 'log H_2A' 'log HA' 'log A^-', ...
      %   'log M^+'};
      % tbl = table(pH, round(logH, 2), round(logOH, 2), round(logH2A, 2), ...
      %   round(logHA, 2), round(logA, 2), round(logM, 2), 'VariableNames', ...
      %   {'pH', 'log_H', 'log_OH','log_H2A', 'log_HA', 'log_A', 'log_M'});

      % Left-hand side of charge balance without metal cation
      LHS = log10(H + 2 * alpha0 .* Ct + alpha1 .* Ct);
      RHS = logOH;
      plt = plot(pH, logH, pH, logOH, pH, logH2A, pH, logHA, pH, logA);
      lgd_str = {'log H^+' 'log OH^-' 'log H_2A' 'log HA' 'log A^-'};
      tbl = table(pH, round(logH, 2), round(logOH, 2), round(logH2A, 2), ...
        round(logHA, 2), round(logA, 2), round(logM, 2), 'VariableNames', ...
        {'pH', 'log_H', 'log_OH','log_H2A', 'log_HA', 'log_A', 'log_M'});
    case 'Weak, monoprotic acid + MB monoprotic salt (same acid/base system)'
      Ct = [str2num(params{3}) str2num(params{6})];
      pKa = str2num(params{4});
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H + Ka(1));
      alpha0 = H ./ d;
      alpha1 = Ka ./ d;
      logHA = log10(alpha0 .* sum(Ct));
      logA = log10(alpha1 .* sum(Ct));
      logM = repmat(log10(Ct(2)), length(pH), 1);

      % Left-hand side of charge balance without metal cation
      LHS = log10(H + Ct(2));
      RHS = log10(OH + alpha1 .* sum(Ct));
      plt = plot(pH, logH, pH, logOH, pH, logHA, pH, logA);
      lgd_str = {'log H^+' 'log OH^-' 'log HA' 'log A^-'};
      tbl = table(pH, round(logH, 2), round(logOH, 2), round(logHA, 2), ...
        round(logA, 2), round(logM, 2), 'VariableNames', {'pH' 'log_H' ...
        'log_OH' 'log_HA' 'log_A' 'log_M'});
    case 'Weak, monoprotic acid + MB monoprotic salt (different acid/base systems)'
      Ct = [str2num(params{3}) str2num(params{6})];
      pKa = [str2num(params{4}) str2num(params{7})];
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H' + Ka)';
      alpha0 = H ./ d;
      alpha1 = Ka' ./ d;
      logHA = log10(alpha0 .* Ct);
      logA = log10(alpha1 .* Ct);
      logM = repmat(log10(Ct(2)), length(pH), 1);

      % Left-hand side of charge balance without metal cation
      LHS = log10(H + alpha0(:, 2) .* Ct(2));
      RHS = log10(OH + alpha1(:, 1) .* Ct(1));
      plt = plot(pH, logH, pH, logOH, pH, logHA(:, 1), pH, logA(:, 1), ...
        pH, logHA(:, 2), pH, logA(:, 2));
      lgd_str = {'log H^+' 'log OH^-' 'log HA' 'log A^{-}' 'log HB' ...
        'log B^{-}'};
      tbl = table(pH, round(logH, 2), round(logOH, 2), round(logHA, 2), ...
        round(logA, 2), round(logM, 2), 'VariableNames', {'pH' 'log_H' ...
        'log_OH' 'log_HA' 'log_A' 'log_M'});
    case 'Weak, monoprotic acid + M2B diprotic salt (different acid/base systems)'
      Ct = [str2num(params{3}) str2num(params{6})];
      pKa = [str2num(params{4}) str2num(params{7}) str2num(params{8})];
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      da = (H + Ka(1));
      alpha0a = H ./ da;
      alpha1a = Ka(1)' ./ da;
      logHA = log10(alpha0a .* Ct(1));
      logA = log10(alpha1a .* Ct(1));
      db = (H .^ 2 + Ka(2) .* H + Ka(2) .* Ka(3));
      alpha0b = H .^ 2 ./ db;
      alpha1b = Ka(2) .* H ./ db;
      alpha2b = Ka(2) .* Ka(3) ./ db;
      logH2B = log10(alpha0b .* Ct(2));
      logHB = log10(alpha1b .* Ct(2));
      logB = log10(alpha2b .* Ct(2));
      logM = repmat(log10(Ct(2)), length(pH), 1);

      % Left-hand side of charge balance without metal cation
      LHS = log10(H + 2 .* alpha0b .* Ct(2) + alpha1b .* Ct(2));
      RHS = log10(OH + alpha1a .* Ct(1));
      lins = [logH logOH logHA logA logH2B logHB logB];
      plt = plot(pH, logH, pH, logOH, pH, logHA, pH, logA, pH, logH2B, ...
        pH, logHB, pH, logB);
      lgd_str = {'log H^+' 'log OH^-' 'log HA' 'log A^{-}' 'log H_2B' ...
        'log HB^{-}' 'log B^{2-}'};
      tbl = table(pH, round(logH, 2), round(logOH, 2), round(logHA, 2), ...
        round(logA, 2), round(logH2B, 2), round(logHB, 2), round(logB, 2), ...
        round(logM, 2), 'VariableNames', {'pH' 'log_H' 'log_OH' 'log_HA' ...
        'log_A' 'log_H2B' 'log_HB' 'log_B' 'log_M'});
    otherwise
      return
  end

  ax = gca;
  ax.XLabel.String = 'pH';
  ax.YLabel.String = 'log C';
  ax.Title.String = 'Log C-pH Diagram';
  ax.YLim = [-14 0];
  ax.XLim = [0 14];

  if (solution)
    hold on
    plot(pH, LHS, '--', 'LineWidth', 2.75);
    plot(pH, RHS, '--', 'LineWidth', 2.75);
    lgd_str = cat(2, lgd_str, {'LHS' 'RHS'});
    ax.Title.String = {'Log C-pH Diagram with Solution', ...
      '( LHS = RHS of Charge Balance )'};
    hold off
  end

  if (titration)
    hold on
    yyaxis right
    titrationplot(params, titr_normal, sample_volume, output);
    ax = gca;
    switch output
      case 'f'
        str = 'f';
        ax.YLabel.String = str;
      case 'g'
        str = 'g';
        ax.YLabel.String = str;
      case 'mL'
        str = 'Titrant volume (mL)';
        ax.YLabel.String = str;
    end
    ymax = 4 .* max(Ct) .* sample_volume ./ titr_normal;
    ax.YLim = [0 ymax];
    lgd_str = cat(2, lgd_str, str);
    if (solution)
      ax.Title.String = {'Log C-pH Diagram with Titration and Solution', ...
        '( LHS = RHS of Charge Balance )'};
    else
      ax.Title.String = 'Log C-pH Diagram with Titration';
    end
    hold off
  end

  lgd = legend(lgd_str);
  lgd.Location = 'southwest';
  grid on;
  grid minor;

end
