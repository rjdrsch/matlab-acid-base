function [plt, tbl] = distributionplot(params)
  sys = params{1};

  pH = (0:0.25:14)';
  H = 10 .^ -pH;
  logH = log10(H);
  pKow = 14;
  logOH = -(pKow + logH);

  switch sys
    case 'Weak, monoprotic acid'
      pKa = str2num(params{4});
      Ka = (10 .^ -pKa)';
      d = (H + Ka(1));
      alpha0 = H ./ d;
      alpha1 = Ka ./ d;
      plt = plot(pH, alpha0, pH, alpha1);
      lgd_str = {'\alpha_0', '\alpha_1'};
      tbl = table(pH, round(alpha0, 4), round(alpha1, 4), ...
        'VariableNames', {'pH', 'alpha0', 'alpha1'});
    case 'Weak, diprotic acid'
      pKa = [str2num(params{4}) str2num(params{5})];
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H .^ 2 + Ka(1) .* H + Ka(1) .* Ka(2));
      alpha0 = H .^ 2 ./ d;
      alpha1 = Ka(1) .* H ./ d;
      alpha2 = Ka(1) .* Ka(2) ./ d;
      plt = plot(pH, alpha0, pH, alpha1, pH, alpha2);
      lgd_str = {'\alpha_0', '\alpha_1', '\alpha_2'};
      tbl = table(pH, round(alpha0, 4), round(alpha1, 4), ...
        round(alpha2, 4), 'VariableNames', {'pH', 'alpha0', 'alpha1', ...
        'alpha2'});
    case {'MB salt (monoprotic acid/base system)', ...
      'Weak, monoprotic acid + MB monoprotic salt (same acid/base system)'}
      pKa = str2num(params{4});
      Ka = (10 .^ -pKa)';
      d = (H + Ka(1));
      alpha0 = H ./ d;
      alpha1 = Ka ./ d;
      plt = plot(pH, alpha0, pH, alpha1);
      lgd_str = {'\alpha_0', '\alpha_1'};
      tbl = table(pH, round(alpha0, 4), round(alpha1, 4), ...
        'VariableNames', {'pH', 'alpha0', 'alpha1'});
    case {'MB salt (diprotic acid/base system)',
      'M2B salt (diprotic acid/base system)'}
      pKa = [str2num(params{4}) str2num(params{5})];
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H .^ 2 + Ka(1) .* H + Ka(1) .* Ka(2));
      alpha0 = H .^ 2 ./ d;
      alpha1 = Ka(1) .* H ./ d;
      alpha2 = Ka(1) .* Ka(2) ./ d;
      plt = plot(pH, alpha0, pH, alpha1, pH, alpha2);
      lgd_str = {'\alpha_0', '\alpha_1', '\alpha_2'};
      tbl = table(pH, round(alpha0, 4), round(alpha1, 4), ...
        round(alpha2, 4), 'VariableNames', {'pH', 'alpha0', 'alpha1', ...
        'alpha2'});
    case 'Weak, monoprotic acid + MB monoprotic salt (different acid/base systems)'
      pKa = [str2num(params{4}) str2num(params{7})];
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H' + Ka)';
      alpha0 = H ./ d;
      alpha1 = Ka' ./ d;
      plt = plot(pH, alpha0(:, 1), pH, alpha1(:, 1), pH, alpha0(:, 2), pH, ...
        alpha1(:, 2));
      lgd_str = {'\alpha_{a,0}', '\alpha_{a,1}', '\alpha_{b,0}', ...
        '\alpha_{b,1}'};
      tbl = table(pH, round(alpha0(:, 1), 4), round(alpha1(:, 1), 4), ...
        round(alpha0(:, 2), 4), round(alpha1(:, 2)), 'VariableNames', ...
        {'pH', 'alpha0_a', 'alpha1_a', 'alpha0_b', 'alpha1_b'});
      case 'Weak, monoprotic acid + M2B diprotic salt (different acid/base systems)'
      pKa = [str2num(params{4}) str2num(params{7}) str2num(params{8})];
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      da = (H + Ka(1));
      alpha0a = H ./ da;
      alpha1a = Ka(1)' ./ da;
      db = (H .^ 2 + Ka(2) .* H + Ka(2) .* Ka(3));
      alpha0b = H .^ 2 ./ db;
      alpha1b = Ka(2) .* H ./ db;
      alpha2b = Ka(2) .* Ka(3) ./ db;
      plt = plot(pH, alpha0a, pH, alpha1a, pH, alpha0b, pH, alpha1b, pH, ...
        alpha2b);
      lgd_str = {'\alpha_{a,0}', '\alpha_{a,1}', '\alpha_{b,0}', ...
        '\alpha_{b,1}' '\alpha_{b,2}'};
      tbl = table(pH, round(alpha0a, 4), round(alpha1a, 4), ...
        round(alpha0b, 4), round(alpha1b, 4), round(alpha2b, 4), ...
        'VariableNames', {'pH', 'alpha0_a', 'alpha1_a', 'alpha0_b', ...
        'alpha1_b' 'alpha2_b'});
    otherwise
      return
  end
  lgd = legend(lgd_str);
  lgd.Location = 'best';
  lgd.FontSize = 14;
  ax = gca;
  ax.Title.String = 'Acid/Base Distribution Diagram';
  ax.XLabel.String = 'pH';
  ax.YLabel.String = 'alpha';
  grid on;
  grid minor;
end
