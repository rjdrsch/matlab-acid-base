function p = getinput()

  % Get acid/base system type
  list = {'Weak, monoprotic acid', 'Weak, diprotic acid', ...
    'MB salt (monoprotic acid/base system)', ...,
    'MB salt (diprotic acid/base system)', ...
    'M2B salt (diprotic acid/base system)', ...,
    'Weak, monoprotic acid + MB monoprotic salt (same acid/base system)', ...
    'Weak, monoprotic acid + MB monoprotic salt (different acid/base systems)', ...
    'Weak, monoprotic acid + M2B diprotic salt (different acid/base systems)'};
  prompt = 'Select acid/base system type and click "Ok".';
  typ = ldg(list, prompt, 'single');
  if (typ < 3)
    p1 = idg(1, list{typ});
    p = cat(1, list{typ}, p1);
  elseif (typ > 2 && typ < 6)
    p1 = idg(1, list{typ});
    p = cat(1, list{typ}, p1);
  elseif (typ == 6)
    p1 = idg(1, list{1});
    p2 = idg(2, list{typ});
    p = cat(1, list{typ}, p1, p2);
  elseif (typ == 7)
    p1 = idg(1, list{1});
    p2 = idg(2, list{3});
    p = cat(1, list{typ}, p1, p2);
  else
    p1 = idg(1, list{1});
    p2 = idg(2, list{5});
    p = cat(1, list{typ}, p1, p2);
  end
end

function x = ldg(list, prompt, mode)

  [x, tf] = listdlg('ListString', list, 'ListSize', [400 200], ...
    'PromptString', prompt, 'SelectionMode', mode);

  if (~tf)
    error('Run cancelled by user.');
  end
end

function y = idg(num, typ)

  s1 = 'Enter species %i. It should be a weak, monoprotic acid.';
  s2 = 'Enter species %i. It should be a weak, diprotic acid.';
  s3 = 'Enter species %i. It should be a salt with a monoprotic acid/base system.';
  s4 = 'Enter species %i. It should be a salt with a diprotic acid/base system.';
  s5 = 'Enter the total concentration (in mol/L) of species %i.';
  s6 = 'Enter the pKa of species %i.';
  s7 = 'Enter pKa 1 of species %i.';
  s8 = 'Enter pKa 2 of species %i.';

  switch typ
    case 'Weak, monoprotic acid'
      cel = {s1, s5, s6};
      def = {'HA', '0.001', '4.76'};
    case 'Weak, diprotic acid'
      cel = {s2, s5, s7, s8};
      def = {'H_{2}CO_{3}', '0.001', '6.3', '10.3'};
    case 'MB salt (monoprotic acid/base system)'
      cel = {s3, s5, s6};
      def = {'NaCN', '0.001', '9.21'};
    case 'MB salt (diprotic acid/base system)'
      cel = {s4, s5, s7, s8};
      def = {'NaHCO_{3}', '0.001', '6.3', '10.3'};
    case 'M2B salt (diprotic acid/base system)'
      cel = {s4, s5, s7, s8};
      def = {'Na_{2}CO_{3}', '0.001', '6.3', '10.3'};
    case 'Weak, monoprotic acid + MB monoprotic salt (same acid/base system)'
      cel = {s3, s5};
      def = {'NaA', '0.001'};
  end

  prompts = cellfun(@(f) sprintf(f, num), cel, 'UniformOut', false);
  y = inputdlg(prompts, '', [1 75], def);
end
