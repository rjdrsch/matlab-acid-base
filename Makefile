VERSION=1.0.1

.PHONY: build
build:
	mkdir matlab-acid-base
	cp *.[mt]* matlab-acid-base/
	zip -r matlab-acid-base-$(VERSION).zip matlab-acid-base
	rm -r matlab-acid-base
