function [plt, tbl] = titrationplot(params, titr_normal, sample_volume, output)
  sys = params{1};

  pH = (0:0.1:14)';
  H = 10 .^ -pH;
  logH = log10(H);
  pKow = 14;
  logOH = -(pKow + logH);
  OH = 10 .^ logOH;

  switch sys
    case 'Weak, monoprotic acid'
      Ct = str2num(params{3});
      pKa = str2num(params{4});
      Ka = (10 .^ -pKa)';
      d = (H + Ka(1));
      alpha0 = H ./ d;
      alpha1 = Ka ./ d;
      f = (OH - H) ./ Ct + alpha1;
      titr_vol = Ct .* sample_volume ./ titr_normal .* f;
      ymax = 4 .* Ct .* sample_volume ./ titr_normal;
      switch output
        case 'f'
          plt = plot(pH, f);
          ax = gca;
          ax.YLabel.String = 'f';
        case 'mL'
          plt = plot(pH, titr_vol);
          ax = gca;
          ax.YLabel.String = 'Titrant volume (mL)';
        otherwise
          error('Must select mL titrant or f output when titrating an acid.');
      end
    case 'MB salt (monoprotic acid/base system)'
      Ct = str2num(params{3});
      pKa = str2num(params{4});
      Ka = (10 .^ -pKa)';
      d = (H + Ka(1));
      alpha0 = H ./ d;
      alpha1 = Ka ./ d;
      g = (H - OH) ./ Ct + alpha0;
      titr_vol = Ct .* sample_volume ./ titr_normal .* g;
      ymax = 4 .* Ct .* sample_volume ./ titr_normal;
      switch output
        case 'g'
          plt = plot(pH, g);
          ax = gca;
          ax.YLabel.String = 'g';
        case 'mL'
          plt = plot(pH, titr_vol);
          ax = gca;
          ax.YLabel.String = 'Titrant volume (mL)';
        otherwise
          error('Must select mL titrant or g output when titrating a base.');
      end
    case 'Weak, diprotic acid'
      Ct = str2num(params{3});
      pKa = [str2num(params{4}) str2num(params{5})];
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H .^ 2 + Ka(1) .* H + Ka(1) .* Ka(2));
      alpha0 = H .^ 2 ./ d;
      alpha1 = Ka(1) .* H ./ d;
      alpha2 = Ka(1) .* Ka(2) ./ d;
      f = (OH - H) ./ Ct + alpha1 + 2 .* alpha2;
      titr_vol = Ct .* sample_volume ./ titr_normal .* f;
      ymax = 4 .* Ct .* sample_volume ./ titr_normal;
      switch output
        case 'f'
          plt = plot(pH, f);
          ax = gca;
          ax.YLabel.String = 'f';
        case 'mL'
          plt = plot(pH, titr_vol);
          ax = gca;
          ax.YLabel.String = 'Titrant volume (mL)';
        otherwise
          error('Must select mL titrant or f output when titrating an acid.');
      end
    case {'MB salt (diprotic acid/base system)', ...
      'M2B salt (diprotic acid/base system)'}
      Ct = str2num(params{3});
      pKa = [str2num(params{4}) str2num(params{5})];
      pKa = sort(pKa);
      Ka = (10 .^ -pKa)';
      d = (H .^ 2 + Ka(1) .* H + Ka(1) .* Ka(2));
      alpha0 = H .^ 2 ./ d;
      alpha1 = Ka(1) .* H ./ d;
      alpha2 = Ka(1) .* Ka(2) ./ d;
      g = (H - OH) ./ Ct + alpha1 + 2 .* alpha0;
      titr_vol = Ct .* sample_volume ./ titr_normal .* g;
      ymax = 4 .* Ct .* sample_volume ./ titr_normal;
      switch output
        case 'g'
          plt = plot(pH, g);
          ax = gca;
          ax.YLabel.String = 'g';
        case 'mL'
          plt = plot(pH, titr_vol);
          ax = gca;
          ax.YLabel.String = 'Titrant volume (mL)';
        otherwise
          error('Must select mL titrant or g output when titrating a base.');
      end
    otherwise
      return
  end
  ax.YLim = [0 ymax];
  ax.Title.String = 'Titration Diagram';
  ax.XLabel.String = 'pH';
  ax.XLim = [0 14];
  grid on;
  grid minor;
end
