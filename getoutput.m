function o = getoutput(params)
  sys = params{1};

  % Get acid/base system type
  list = {'Solver', 'Beaker Diagram', 'Distribution Diagram', ...
    'Log C-pH Diagram', 'Log C-pH Diagram (with solution)', ...
    'Log C-pH Diagram (with titration)', 'Titration Diagram'};

  switch sys
    case {'Weak, monoprotic acid + MB monoprotic salt (same acid/base system)', ...
    'Weak, monoprotic acid + MB monoprotic salt (different acid/base systems)', ...
    'Weak, monoprotic acid + M2B diprotic salt (different acid/base systems)'};
    list = {list{1:(end - 2)}};
  end

  prompt = 'Select output modules and click "Ok".';
  types = listdlg('ListString', list, 'ListSize', [300 200], 'PromptString', ...
    prompt);

  if (any(types == 2))
    species = sdg();
  end

  if (any(types > 5))
    switch sys
      case {'Weak, monoprotic acid', 'Weak, diprotic acid'}
        str = {'Enter titration normality.', 'Enter sample volume in mL.', ...
          'Enter titration output (mL titrant or f)'};
      case {'MB salt (monoprotic acid/base system)', ...
        'MB salt (diprotic acid/base system)', ...
        'M2B salt (diprotic acid/base system)'}
        str = {'Enter titration normality.', 'Enter sample volume in mL.', ...
          'Enter titration output (mL titrant or g)'};
    end
    titr = inputdlg(str, '', [1 75], {'0.1', '100', 'mL'});
    titr_normal = str2num(titr{1});
    sample_volume = str2num(titr{2});
    titr_output = titr{3};
  end

  if (length(types) == 1)
    types = {list{types}};
  else
    types = list(types);
  end
  n = length(types);
  while (n > 0)
    typ = types{n};
    switch typ
      case 'Solver'
        solver(params)
      case 'Beaker Diagram'
        figure;
        beakerplot(params, species)
      case 'Distribution Diagram'
        figure;
        distributionplot(params);
      case 'Log C-pH Diagram'
        figure;
        logCpH(params, false, false);
      case 'Log C-pH Diagram (with solution)'
        figure;
        logCpH(params, true, false);
      case 'Log C-pH Diagram (with titration)'
        figure;
        logCpH(params, true, true, titr_normal, sample_volume, titr_output);
      case 'Titration Diagram'
        figure;
        titrationplot(params, titr_normal, sample_volume, titr_output);
    end
  n = n - 1;
  end
end

function z = sdg()

  list = {'A^{-}', 'Cl^-', 'CO_{3}^{2-}', 'CN^{-}', 'F^{-}', 'HA', ...
    'HCO_{3}^{-}', 'HCN', 'HF', 'HOCl', 'H^{+}', 'HS^{-}', 'H_{2}CO_{3}', ...
    'H_{2}O', 'H_{2}S','HSO_{4}^{-}', 'K^{+}', 'Li^{+}', 'Na^{+}', 'OH^{-}', ...
    'OCl^{-}', 'S^{2-}', 'SO_{4}^{2-}'};
  list = sort(list);
  s1 = 'Select all the dissolved species in your water.';
  s2 = 'Hold control in Windows and command in Mac to select multiple species.';
  s3 = 'We''ve already selected two to get you started.';
  s4 = 'Note: superscripts are indicated by ^{ } and subscripts _{ }.';
  prompt = {s1 '' cat(2, s2, ' ', s3), '', s4, ''};
  [z, tf] = listdlg('ListString', list, 'ListSize', [300 300], ...
    'PromptString', prompt, 'InitialValue', [13 15 21]);

  if (~tf)
    error('Run cancelled by user.');
  end

  z = list(z)';
end
