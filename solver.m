% Solve system of equations based on params
function x = solver(params)
  sys = params{1};

  fun = @(z) get_sys_eqns(z, params);
  opts = optimoptions('fsolve', 'MaxFunctionEvaluations', 10000, ...
    'MaxIterations', 5000, 'Display', 'none');
  switch sys
    case 'Weak, monoprotic acid'
      Ct = str2num(params{3});
      x0 = [3.5 -9.5 Ct -5];
      out = fsolve(fun, x0, opts);
      x = array2table(out, 'VariableNames', {'pH', 'log_OH', 'log_HA', ...
        'log_A'});
    case 'Weak, diprotic acid'
      Ct = str2num(params{3});
      x0 = [8.5 -5.5 -4 Ct -4];
      out = fsolve(fun, x0, opts);
      x = array2table(out, 'VariableNames', {'pH', 'log_OH', 'log_H2A', ...
        'log_HA', 'log_A'});
    case 'MB salt (monoprotic acid/base system)'
      Ct = str2num(params{3});
      x0 = [10 -4 -5 Ct Ct];
      out = fsolve(fun, x0, opts);
      x = array2table(out, 'VariableNames', {'pH', 'log_OH', 'log_HA', ...
        'log_A', 'log_M'});
    case 'M2B salt (monoprotic acid/base system)'
      Ct = str2num(params{3});
      x0 = [10.5 -3.5 -5 Ct Ct];
      out = fsolve(fun, x0, opts);
      x = array2table(out, 'VariableNames', {'pH', 'log_OH', 'log_HA', ...
        'log_A', 'log_M'});
    case {'MB salt (diprotic acid/base system)', ...
      'M2B salt (diprotic acid/base system)'}
      Ct = str2num(params{3});
      x0 = [8.5 -5.5 -4 Ct -4 Ct];
      out = fsolve(fun, x0, opts);
      x = array2table(out, 'VariableNames', {'pH', 'log_OH', 'log_H2A', ...
        'log_HA', 'log_A', 'log_M'});
    case 'Weak, monoprotic acid + MB monoprotic salt (same acid/base system)'
      Ct = [str2num(params{3}) str2num(params{6})];
      x0 = [7 -7 -4 -4 Ct(2)];
      out = fsolve(fun, x0, opts);
      x = array2table(out, 'VariableNames', {'pH', 'log_OH', 'log_HA', ...
        'log_A', 'log_M'});
    case 'Weak, monoprotic acid + MB monoprotic salt (different acid/base systems)'
      Ct = [str2num(params{3}) str2num(params{6})];
      x0 = [6.5 -6.5 -5 -3 -3 -5 Ct(2)];
      out = fsolve(fun, x0, opts);
      x = array2table(out, 'VariableNames', {'pH', 'log_OH', 'log_HA', ...
        'log_A', 'log_HB', 'log_B', 'log_M'});
    case 'Weak, monoprotic acid + M2B diprotic salt (different acid/base systems)'
      Ct = [str2num(params{3}) str2num(params{6})];
      x0 = [6.5 -6.5 -5 -3 -4 -3 -4 Ct(2)];
      out = fsolve(fun, x0, opts);
      x = array2table(out, 'VariableNames', {'pH', 'log_OH', 'log_HA', ...
        'log_A', 'log_H2B', 'log_HB' 'log_B', 'log_M'});
  end
end

% Get system of equations based on params
function f = get_sys_eqns(x, params)
  sys = params{1};

  pKa_H2O = 14;

  switch sys
    case 'Weak, monoprotic acid'
      Ct = str2num(params{3});
      pKa = str2num(params{4});
      % Equation 1: Mass law for water
      f(1) = x(1) - x(2) - pKa_H2O;
      % Equation 2: Mass law for acid/base system
      f(2) = x(1) - x(4) + x(3) - pKa;
      % Equation 3: Mass balance based on total weak acid added
      f(3) = 10 .^ x(3) + 10 .^ x(4) - Ct;
      % Equation 4: Charge balance
      f(4) = 1 - 10 .^ -x(1) / (10 .^ x(2) + 10 .^ x(4));
    case 'Weak, diprotic acid'
      Ct = str2num(params{3});
      pKa = [str2num(params{4}) str2num(params{5})]';
      pKa = sort(pKa);
      % Equation 1: Mass law for water
      f(1) = x(1) - x(2) - pKa_H2O;
      % Equation 2: Mass law for first pKa
      f(2) = x(1) - x(4) + x(3) - pKa(1);
      % Equation 3: Mass law for second pKa
      f(3) = x(1) - x(5) + x(4) - pKa(2);
      % Equation 4: Mass balance based on total weak acid added
      f(4) = 10 .^ x(3) + 10 .^ x(4) + 10 .^ x(5) - Ct;
      % Equation 5: Charge balance
      f(5) = 1 - 10 .^ -x(1) / (10 .^ x(2) + 10 .^ x(4) + 2 * 10 .^ x(5));
    case 'MB salt (monoprotic acid/base system)'
      Ct = str2num(params{3});
      pKa = str2num(params{4});
      % Equation 1: Mass law for water
      f(1) = x(1) - x(2) - pKa_H2O;
      % Equation 2: Mass law for acid/base system
      f(2) = x(1) - x(4) + x(3) - pKa;
      % Equation 3: Mass balance based on total base added
      f(3) = 10 .^ x(3) + 10 .^ x(4) - Ct;
      % Equation 4: Mass balance based on metal cation
      f(4) = 10 .^ x(5) - Ct;
      % Equation 5: Charge balance
      f(5) = 1 - (10 .^ -x(1) + 10 .^ x(5)) / (10 .^ x(2) + 10 .^ x(4));
    case 'M2B salt (monoprotic acid/base system)'
      Ct = str2num(params{3});
      pKa = str2num(params{4});
      % Equation 1: Mass law for water
      f(1) = x(1) - x(2) - pKa_H2O;
      % Equation 2: Mass law for acid/base system
      f(2) = x(1) - x(4) + x(3) - pKa;
      % Equation 3: Mass balance based on total base added
      f(3) = 10 .^ x(3) + 10 .^ x(4) - Ct;
      % Equation 4: Mass balance based on metal cation
      f(4) = 10 .^ x(5) - 2 .* Ct;
      % Equation 5: Charge balance
      f(5) = 1 - (10 .^ -x(1) + 10 .^ x(5)) / (10 .^ x(2) + 10 .^ x(4));
    case 'MB salt (diprotic acid/base system)'
      Ct = str2num(params{3});
      pKa = [str2num(params{4}) str2num(params{5})]';
      pKa = sort(pKa);
      % Equation 1: Mass law for water
      f(1) = x(1) - x(2) - pKa_H2O;
      % Equation 2: Mass law for first pKa
      f(2) = x(1) - x(4) + x(3) - pKa(1);
      % Equation 3: Mass law for second pKa
      f(3) = x(1) - x(5) + x(4) - pKa(2);
      % Equation 4: Mass balance based on total base acid added
      f(4) = 10 .^ x(3) + 10 .^ x(4) + 10 .^ x(5) - Ct;
      % Equation 5: Mass balance based on metal cation
      f(5) = 10 .^ x(6) - Ct;
      % Equation 6: Charge balance
      f(6) = 1 - (10 .^ -x(1) + 10 .^ x(6)) / (10 .^ x(2) + 10 .^ x(4) + ...
        2 * 10 .^ x(5));
    case 'M2B salt (diprotic acid/base system)'
      Ct = str2num(params{3});
      pKa = [str2num(params{4}) str2num(params{5})]';
      pKa = sort(pKa);
      % Equation 1: Mass law for water
      f(1) = x(1) - x(2) - pKa_H2O;
      % Equation 2: Mass law for first pKa
      f(2) = x(1) - x(4) + x(3) - pKa(1);
      % Equation 3: Mass law for second pKa
      f(3) = x(1) - x(5) + x(4) - pKa(2);
      % Equation 4: Mass balance based on total base acid added
      f(4) = 10 .^ x(3) + 10 .^ x(4) + 10 .^ x(5) - Ct;
      % Equation 5: Mass balance based on metal cation
      f(5) = 10 .^ x(6) - 2 .* Ct;
      % Equation 6: Charge balance
      f(6) = 1 - (10 .^ -x(1) + 10 .^ x(6)) / (10 .^ x(2) + 10 .^ x(4) + ...
        2 * 10 .^ x(5));
    case 'Weak, monoprotic acid + MB monoprotic salt (same acid/base system)'
      Ct = [str2num(params{3}) str2num(params{6})];
      pKa = str2num(params{4});
      % Equation 1: Mass law for water
      f(1) = x(1) - x(2) - pKa_H2O;
      % Equation 2: Mass law for first acid/base system
      f(2) = x(1) - x(4) + x(3) - pKa(1);
      % Equation 3: Mass balance based on the acid/base system
      f(3) = 10 .^ x(3) + 10 .^ x(4) - Ct(1) - Ct(2);
      % Equation 4: Mass balance based on metal cation
      f(4) = 10 .^ x(5) - Ct(2);
      % Equation 5: Charge balance
      f(5) = 1 - (10 .^ -x(1) + 10 .^ x(5)) / (10 .^ x(2) + 10 .^ x(4));
    case 'Weak, monoprotic acid + MB monoprotic salt (different acid/base systems)'
      Ct = [str2num(params{3}) str2num(params{6})];
      pKa = [str2num(params{4}) str2num(params{7})]';
      % Equation 1: Mass law for water
      f(1) = x(1) - x(2) - pKa_H2O;
      % Equation 2: Mass law for first acid/base system
      f(2) = x(1) - x(4) + x(3) - pKa(1);
      % Equation 3: Mass law for second acid/base system
      f(3) = x(1) - x(6) + x(5) - pKa(2);
      % Equation 4: Mass balance based on total first acid/base system
      f(4) = 10 .^ x(3) + 10 .^ x(4) - Ct(1);
      % Equation 5: Mass balance based on total first acid/base system
      f(5) = 10 .^ x(5) + 10 .^ x(6) - Ct(2);
      % Equation 5: Mass balance based on metal cation
      f(6) = 10 .^ x(7) - Ct(2);
      % Equation 6: Charge balance
      f(7) = 1 - (10 .^ -x(1) + 10 .^ x(7)) / (10 .^ x(2) + 10 .^ x(4) +  ...
        10 .^ x(6));
    case 'Weak, monoprotic acid + M2B diprotic salt (different acid/base systems)'
      Ct = [str2num(params{3}) str2num(params{6})];
      pKa = [str2num(params{4}) str2num(params{7}) str2num(params{8})];
      % Equation 1: Mass law for water
      f(1) = x(1) - x(2) - pKa_H2O;
      % Equation 2: Mass law for first acid/base system
      f(2) = x(1) - x(4) + x(3) - pKa(1);
      % Equation 3: First Mass law for diprotic acid/base system
      f(3) = x(1) - x(6) + x(5) - pKa(2);
      % Equation 4: Second Mass law for diprotic acid/base system
      f(4) = x(1) - x(7) + x(6) - pKa(3);
      % Equation 5: Mass balance based on total first acid/base system
      f(5) = 10 .^ x(3) + 10 .^ x(4) - Ct(1);
      % Equation 6: Mass balance based on total first acid/base system
      f(6) = 10 .^ x(5) + 10 .^ x(6) + 10 .^ x(7) - Ct(2);
      % Equation 7: Mass balance based on metal cation
      f(7) = 10 .^ x(8) - 2 .* Ct(2);
      % Equation 8: Charge balance
      f(8) = 1 - (10 .^ -x(1) + 2 .* 10 .^ x(5) + 10 .^ x(6)) / (10 .^ x(2) ...
        + 10 .^ x(4));
  end
end
