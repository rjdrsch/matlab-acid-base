function beakerplot(params, species)
  sys = params{1};

  ex = 0.2 * randn(length(species), 1)';
  x = (1:length(species)) + ex;
  ey = 25 * randn(length(species), 1)';
  y = repmat(150, 1, length(species)) + ey;

  fill([0 0 length(x) + 1 length(x) + 1], [0 250 250 0], 'b', 'FaceAlpha', ...
    0.05, 'EdgeColor', 'none');
  hold on
  plot(x, y, 'o');
  text(x, y - 20, species, 'FontSize', 12);

  switch sys
    case 'Weak, monoprotic acid'
      recipe = params{2};
      x2 = [0.5 0.5];
      y2 = [375 275] ./ 500;
      annotation('textarrow', x2, y2, 'String', recipe, 'FontSize', 12, ...
        'HorizontalAlignment', 'center');
    case 'Weak, diprotic acid'
      recipe = params{2};
      x2 = [0.5 0.5];
      y2 = [375 275] ./ 500;
      annotation('textarrow', x2, y2, 'String', recipe, 'FontSize', 12, ...
        'HorizontalAlignment', 'center');
    case 'MB salt (monoprotic acid/base system)'
      recipe = {params{2}, '(completely dissociates)'};
      x2 = [0.5 0.5];
      y2 = [375 275] ./ 500;
      annotation('textarrow', x2, y2, 'String', recipe, 'FontSize', 12, ...
        'HorizontalAlignment', 'center');
    case {'MB salt (diprotic acid/base system)', ...
      'M2B salt (diprotic acid/base system)'}
      recipe = {params{2}, '(completely dissociates)'};
      x2 = [0.5 0.5];
      y2 = [375 275] ./ 500;
      annotation('textarrow', x2, y2, 'String', recipe, 'FontSize', 12, ...
        'HorizontalAlignment', 'center');
    case {'Weak, monoprotic acid + MB monoprotic salt (same acid/base system)', ...
      'Weak, monoprotic acid + MB monoprotic salt (different acid/base systems)', ...
      'Weak, monoprotic acid + M2B diprotic salt (different acid/base systems)'}
      recipe = params{2};
      x2 = [0.3 0.3];
      y2 = [375 275] ./ 500;
      recipe2 = {params{5}, '(completely dissociates)'};
      annotation('textarrow', x2, y2, 'String', recipe, 'FontSize', 12, ...
        'HorizontalAlignment', 'center');
      annotation('textarrow', x2 + 0.4, y2, 'String', recipe2, 'FontSize', 12, ...
        'HorizontalAlignment', 'center');
  end

  ax = gca;
  ax.XLim = [0 length(species) + 1];
  ax.XTick = [];
  ax.XTickLabels = {};
  ax.YLim = [0 500];
  ax.YLabel.String = 'Beaker volume (mL)';
  hold off
end
