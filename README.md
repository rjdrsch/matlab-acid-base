# matlab-acid-base

Solve acid/base equilibria using [MATLAB](https://www.mathworks.com/matlab.html).

**Version**: 1.0.2

**Copyright**: R. Justin Davis, Lynn E. Katz (2019-present)

**Maintainer**: R. Justin Davis (<jdavis.agua@gmail.com>)


## License

These scripts are distributed under the [GNU General Public License, Version 3
(GPLv3)](https://choosealicense.com/licenses/gpl-3.0/#). The package may be
reused or modified with the following conditions: Copyright and license notices
must be preserved, any modifications must be specified, and the complete source
code must be made available under the same license (GPLv3).

## Description

This repository contains several modules for exploring different acid/base
equilibria. Currently, the package can accommodate the following acid/base systems:

1. Weak, monoprotic acid (HA)
2. Weak, diprotic acid (H2A)
3. Salt (of the general formula MB) for a monoprotic acid/base system
4. Salt (of the general formula MB) for a diprotic acid/base system
5. Salt (of the general formula M2B) for a diprotic acid/base system
6. Weak, monoprotic acid + MB monoprotic salt (same acid/base system)
7. Weak, monoprotic acid + MB monoprotic salt (different acid/base systems)
8. Weak, monoprotic acid + M2B diprotic salt (different acid/base systems)

The following table describes available outputs:

| Output                         | Description                                                         |
| :----------------------------- | :------------------------------------------------------------------ |
| Beaker plot                    | Predict species in the "beaker" of water at equilibrium             |
| Distribution plot              | Plots species distribution as a function of pH                      |
| log C-pH plot                  | Plots log C vs. pH for each species at equilibrium                  |
| log C-pH plot (with solution)  | Log C vs. pH with graphical solution                                |
| log C-pH plot (with titration) | Log C vs. pH with titration plot                                    |                
| Titration plot                 | Plots acid or base titration for a given system                     |
| Solver                         | Solves equilibrium concentrations using a nonlinear solver          |

NOTE: Titration plots are not available for acid + salt systems (options 6-8
in the acid/base systems above).

## Installation

First, make sure you have the following dependencies installed:

* [MATLAB](https://www.mathworks.com/matlab.html)
* [MATLAB Optimization Toolbox](https://www.mathworks.com/products/optimization.html)
* [MATLAB Statistics and Machine Learning Toolbox](https://www.mathworks.com/products/statistics.html?s_tid=srchtitle)

The easiest way to install toolboxes is through the MATLAB interface. Go the the
`Home` tab in MATLAB, and click `Add-Ons`, then `Get Add-Ons`. This will bring
up a new window containing installable addons for MATLAB. Search for the
appropriate toolbox in the search bar, and click the toolbox title to bring up
its description. On the description page, there will be a button that says
`Install` or `Sign in to Install`. Click that button and then follow the
prompts to complete installation.

After you've installed the dependencies, the easiest way to obtain
`matlab-acid-base` is to download the .zip file. To do so, click the download
icon on the navigation bar above (next to the 'Clone' button), and then click on
.zip file. Once the file is downloaded, you can unzip `matlab-acid-base` into a
directory on your MATLAB path. Alternatively, you can navigate to the
`matlab-acid-base` directory in the 'Current Folder' window in MATLAB.

If you use [Git](https://git-scm.com), you may open up a terminal application
and type

``` sh
cd /path/to/your/folder
git clone https://gitlab.com/rjustindavis/matlab-acid-base.git
```

where you substitute `/path/to/your/folder` for an absolute path on your
computer. Afterwards, you can navigate to the cloned directory inside the
'Current Folder' window in MATLAB.

## Usage

The package can be used one of two ways: interactively or manually. To run the
code interactively, ensure the `matlab-acid-base` folder is on your MATLAB path
or selected as the 'Current Folder' in MATLAB. Then, type the following line
into the MATLAB Command Window:

``` matlab
main
```

A dialog window will pop up asking to specify the acid/base system (see options
in table above. After you select the system, you'll be prompted to enter
species concentration information. These dialogs are populated with example
species and concentrations, but you can change them according to the recipe of
your problem. After you enter species information, you'll be asked to select
output options (shown in table above). Multiple options can be specified in a
single run.

## Distribution

If distributed or used in a classroom, please follow the installation
instructions above. If this is not possible, please include the package in an
archive (e.g., a zip file) to ensure the README and LICENSE files are intact. To
build the zip file, simply open a terminal application and type the following
command:

``` sh
cd /path/to/your/folder
make
```

where again, `/path/to/your/folder` must be substituted for an absolute path on
your computer. This will create a zip file with the current version number.
