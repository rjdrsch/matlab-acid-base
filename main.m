%% Get user data
params = getinput();

%% Interactive plotting
getoutput(params);

%% Manual plotting
%
% Uncomment to solve system of equations
% solver(params);

% Uncomment to make beaker diagram
% beakerplot(params);

% Uncomment to make distribution diagram
% distributionplot(params);

% Uncomment to make log C-pH diagram
% logCpH(params);

% Uncomment to make log C-pH diagram with solution
% logCpH(params, true);

% Uncomment to make titration diagrams
% titr_normal = 0.1; % eq/L
% sample_volume = 100; % mL
% titr_output = 'f'; % Can be f or mL
% titrationplot(params)
% logCpH(params, true)

% Uncomment to make distribution diagram
% distributionplot(params)
